package com.example.emir.placefinder.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.emir.placefinder.R;

public class CafesFragment extends Fragment {

    private TextView title;
    
    public CafesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View cafesViews = inflater.inflate(R.layout.fragment_cafes, container, false);

        title = cafesViews.findViewById(R.id.cafes_title);


        return cafesViews;
    }

}

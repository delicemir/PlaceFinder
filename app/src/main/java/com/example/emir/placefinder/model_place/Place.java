package com.example.emir.placefinder.model_place;

import com.google.android.gms.maps.model.LatLng;

public class Place {

    private String id;
    private String name;
    private LatLng location;
    private String iconUrl;
    private Boolean isOpenNow;
    private String place_id;
    private Float rating;
    private String vicinity;

    public Place() {
    }

    public Place(String id, String name, LatLng location, String iconUrl,
                 Boolean isOpenNow, String place_id, Float rating, String vicinity) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.iconUrl = iconUrl;
        this.isOpenNow = isOpenNow;
        this.place_id = place_id;
        this.rating = rating;
        this.vicinity = vicinity;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LatLng getLocation() {
        return location;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public Boolean getOpenNow() {
        return isOpenNow;
    }

    public String getPlace_id() {
        return place_id;
    }

    public Float getRating() {
        return rating;
    }

    public String getVicinity() {
        return vicinity;
    }

    @Override
    public String toString() {
        return "Place{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", location=" + location +
                ", iconUrl='" + iconUrl + '\'' +
                ", isOpenNow=" + isOpenNow +
                ", place_id='" + place_id + '\'' +
                ", rating=" + rating +
                ", vicinity='" + vicinity + '\'' +
                '}';
    }
}

package com.example.emir.placefinder;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.emir.placefinder.constants.AppConstants;
import com.example.emir.placefinder.user.User;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.jakewharton.threetenabp.AndroidThreeTen;

import org.threeten.bp.LocalDateTime;

import java.security.MessageDigest;
import java.util.Arrays;

public class LoginActivity extends AppCompatActivity {

    private LoginButton loginButton;
    private ImageView loginImage;
    private LinearLayout loginHolder;
    private EditText userUsername;
    private EditText userPassword;
    private Button loginBtn;

    private ProgressBar progressBarLogin;
    private CallbackManager callbackManager;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    private LocalDateTime dateTimeNow;
    private User newUser;

    private SharedPreferences sharedPreferences;

    private Boolean isUserLoggedIn = false;

    private MediaPlayer mp, startupSound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidThreeTen.init(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        sharedPreferences = this.getSharedPreferences(AppConstants.SHARED_PREFERENCES, MODE_PRIVATE);
        isUserLoggedIn = sharedPreferences.getBoolean("isUserLoggedIn",false);
        if(isUserLoggedIn) {
            goToMain();
        }

        initialize();

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        loginButton.setReadPermissions(Arrays.asList("public_profile", "user_hometown", "email", "user_location", "user_birthday"));

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                showAlertDialog("Login", "Login attempt canceled by user.");
                progressBarLogin.setVisibility(View.GONE);
                loginButton.setClickable(true);
            }

            @Override
            public void onError(FacebookException error) {
                showAlertDialog("Login denied","Login attempt failed, permission not granted!");
                progressBarLogin.setVisibility(View.GONE);
                loginButton.setClickable(true);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mp.start();
                if(!isUserLoggedIn) {
                    progressBarLogin.setVisibility(View.VISIBLE);
                    loginButton.setClickable(false);
                }
            }
        });

    }

    private void showAlertDialog(String title, String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("Okay",null);
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void handleFacebookAccessToken(AccessToken token) {

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            if(user == null) {
                                return;
                            }
                            Toast.makeText(getApplicationContext(), "Welcome " + user.getDisplayName(),
                                    Toast.LENGTH_LONG).show();
                            saveUser(user);
                            sharedPreferences.edit().putBoolean(AppConstants.SHARED_LOGIN, true).apply();
                            progressBarLogin.setVisibility(View.GONE);
                            startupSound.start();
                            goToMain();
                        }
                        else {
                            progressBarLogin.setVisibility(View.GONE);
                            showAlertDialog("Firebase error!", "Authentication failed.");
                            saveUser(null);
                        }
                    }
                });
    }

    private void goToMain() {
        Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
        startActivity(intent);

        finish();

    }

    private void saveUser(FirebaseUser user) {

        if (user == null) {
            showAlertDialog("Login","User signed out...");
            return;
        }

        dateTimeNow = LocalDateTime.now();
        newUser = new User(user.getDisplayName(),user.getEmail(),user.getPhoneNumber(),user.getPhotoUrl().toString(),dateTimeNow.toString());
        mDatabase.child("users").child(user.getUid()).setValue(newUser);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        saveUser(mAuth.getCurrentUser());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void initialize() {
        loginButton = findViewById(R.id.login_button);
        loginImage = findViewById(R.id.login_img);
        loginHolder = findViewById(R.id.login_holder);
        progressBarLogin = findViewById(R.id.progress_bar_login);
        userUsername = findViewById(R.id.login_username);
        userPassword = findViewById(R.id.login_password);
        loginBtn = findViewById(R.id.login_btn);

        progressBarLogin.setVisibility(View.GONE);
        // Initialize Firebase Auth and Database
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        loginButton.setClickable(true);

        mp = MediaPlayer.create(this,R.raw.button_click);
        startupSound = MediaPlayer.create(this,R.raw.startup);

        //generateFbFingerPrint();

    }

    public void generateFbFingerPrint() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.example.emir.placefinder", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String sign = Base64
                        .encodeToString(md.digest(), Base64.DEFAULT);
                Log.e("KEYHASH:", sign);
                //System.out.println("Keyhash "+sign);
                //Toast.makeText(getApplicationContext(), sign, Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void login() {

        mp.start();
        if(!validateLogin()) {
            Toast.makeText(getApplicationContext(),"Not valid login!", Toast.LENGTH_LONG).show();
            return;
        }
        sharedPreferences.edit().putBoolean(AppConstants.SHARED_LOGIN, true).apply();
        startupSound.start();
        goToMain();

    }

    private boolean validateLogin() {
        if(!userUsername.getText().toString().isEmpty() && !userPassword.getText().toString().isEmpty()) {
            return true;
        }
        Toast.makeText(getApplicationContext(), "You must enter username and password!", Toast.LENGTH_SHORT).show();
        return false;
    }
}

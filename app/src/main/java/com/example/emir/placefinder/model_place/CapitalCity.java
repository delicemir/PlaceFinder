package com.example.emir.placefinder.model_place;

import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;

public class CapitalCity implements Comparable{

    private String cityName;
    private LatLng cityCoordinates;

    public CapitalCity() {
    }

    public CapitalCity(String cityName, LatLng cityCoordinates) {
        this.cityName = cityName;
        this.cityCoordinates = cityCoordinates;
    }

    public String getCityName() {
        return cityName;
    }

    public LatLng getCityCoordinates() {
        return cityCoordinates;
    }

    @Override
    public String toString() {
        return "CapitalCity{" +
                "cityName='" + cityName + '\'' +
                ", cityCoordinates=" + cityCoordinates +
                '}';
    }

    @Override
    public int compareTo(@NonNull Object o) {
        CapitalCity city = (CapitalCity) o;
        return getCityName().compareTo(city.getCityName());
    }
}

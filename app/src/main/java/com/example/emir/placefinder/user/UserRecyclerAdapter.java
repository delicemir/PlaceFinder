package com.example.emir.placefinder.user;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import com.example.emir.placefinder.R;
import com.squareup.picasso.Picasso;

import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

public class UserRecyclerAdapter extends RecyclerView.Adapter<UserRecyclerAdapter.ViewHolder> {

    List<User> userList = new ArrayList<>();
    Context context;

    public UserRecyclerAdapter(List<User> userList, Context context) {
        this.userList = userList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_recycler_item_view, parent, false);
        return new UserRecyclerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        User user = userList.get(position);

        holder.userName.setText(user.getDisplayName());
        holder.userEmail.setText("Email: "+user.getUserEmail());
        holder.userLoginDateTime.setText("Last login at: "+getFormattedDateTime(user.getLoginAt()));
        Picasso.with(context).load(user.getUserPhotoUrl())
                .placeholder(R.drawable.person_placeholder)
                .into(holder.userPhoto);
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView userName;
        private ImageView userPhoto;
        private TextView userEmail;
        private TextView userLoginDateTime;


        public ViewHolder(View itemView) {
            super(itemView);

            userName = itemView.findViewById(R.id.user_recycler_name);
            userEmail = itemView.findViewById(R.id.user_recycler_email);
            userLoginDateTime = itemView.findViewById(R.id.user_recycler_last_login);
            userPhoto = itemView.findViewById(R.id.user_recycler_img);
        }
    }

    private String getFormattedDateTime(String lastLoginAt) {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        LocalDateTime ldt = LocalDateTime.parse(lastLoginAt);

        return dtf.format(ldt);
    }
}

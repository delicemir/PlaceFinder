package com.example.emir.placefinder.constants;

import com.google.android.gms.maps.model.LatLng;

public class AppConstants {

    // samo radijus i kljuc
    public static final String URL_BASE = "https://maps.googleapis.com/maps/api/place/nearbysearch/json" +
            "?radius=1000" +
            "&key=AIzaSyBrJwh3T0lVnzcclC8sV9rREHKIUHCzwPw";


    public static final String URL_PLACES_RESTAURANT = "https://maps.googleapis.com/maps/api/place/nearbysearch/json" +
            "?radius=1000" +
            "&type=restaurant" +
            "&key=AIzaSyBrJwh3T0lVnzcclC8sV9rREHKIUHCzwPw";

    public static final String URL_PLACES_HOTELS = "https://maps.googleapis.com/maps/api/place/nearbysearch/json" +
            "?radius=1000" +
            "&type=lodging" +
            "&key=AIzaSyBrJwh3T0lVnzcclC8sV9rREHKIUHCzwPw";

    public static final String URL_PLACES_CAFES = "https://maps.googleapis.com/maps/api/place/nearbysearch/json" +
            "?radius=1000" +
            "&type=cafe" +
            "&key=AIzaSyBrJwh3T0lVnzcclC8sV9rREHKIUHCzwPw";


    //Tuzla = >   &location=44.537064,18.678409
    public static final LatLng DEFAULT_LOCATION = new LatLng(44.537064,18.678409);

    public static final String URL_CAPITAL_CITIES = "http://www.geognos.com/api/en/countries/info/all.json";


    public static final String SHARED_PREFERENCES = "com.example.emir.placefinder";
    public static final String SHARED_LOGIN = "isUserLoggedIn";


}

package com.example.emir.placefinder;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.emir.placefinder.model_place.Place;
import com.squareup.picasso.Picasso;

public class PlaceInfoDialog extends DialogFragment {

    private TextView placeInfoTitle;
    private ImageView placeInfoIcon;
    private TextView placeInfoName;
    private TextView placeInfoRating;
    private RatingBar ratingBar;
    private TextView placeInfoOpenNow;
    private Button btnOkay;

    private static Place placeInfo;
    private static Context contextInfo;

    public PlaceInfoDialog() {
    }

    public static PlaceInfoDialog newInstance(Place place, Context context) {

        placeInfo = place;
        contextInfo = context;

        return new PlaceInfoDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.preview_place_info,container,false);

        placeInfoTitle = view.findViewById(R.id.place_info_title);
        placeInfoIcon = view.findViewById(R.id.place_info_icon);
        placeInfoName = view.findViewById(R.id.place_info_name);
        placeInfoRating = view.findViewById(R.id.place_info_rating);
        ratingBar = view.findViewById(R.id.rating_bar);
        placeInfoOpenNow = view.findViewById(R.id.place_open_now);
        btnOkay = view.findViewById(R.id.place_info_btn_okay);

        setPlaceInfo();

        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        return view;
    }

    private void setPlaceInfo() {

        placeInfoName.setText(placeInfo.getName());
        if(placeInfo.getRating().isNaN() || placeInfo.getRating().isInfinite()) {
            placeInfoRating.setText("Nema ocjene!");
            ratingBar.setRating(0);
        }
        else {
            placeInfoRating.setText("Ocjena: "+String.valueOf(placeInfo.getRating()));
            ratingBar.setRating(placeInfo.getRating());
        }

        if(placeInfo.getOpenNow()==null) {
            placeInfoOpenNow.setText("Otvoreno ? : Nije poznato!");
        }
        else if(placeInfo.getOpenNow()) {
            placeInfoOpenNow.setText("Trenutno otvoreno.");
        }
        else {
            placeInfoOpenNow.setText("Trenutno zatvoreno.");
        }
        Picasso.with(contextInfo).load(placeInfo.getIconUrl()).into(placeInfoIcon);

    }
}

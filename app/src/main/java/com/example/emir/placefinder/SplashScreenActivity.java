package com.example.emir.placefinder;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SplashScreenActivity extends AppCompatActivity {

    private TextView splashText;
    private ImageView imgSplash;
    private LinearLayout mainHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash_screen);

        splashText = findViewById(R.id.text_splash);
        imgSplash = findViewById(R.id.img_splash);
        mainHolder = findViewById(R.id.main_holder_splash);

        Animation splashAnimation = AnimationUtils.loadAnimation(this,R.anim.animate);
        mainHolder.startAnimation(splashAnimation);

        int secondsDelayed = 4;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
                finish();
            }
        }, secondsDelayed * 1000);


    }
}

package com.example.emir.placefinder.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.emir.placefinder.R;

public class RestaurantsFragment extends Fragment {

    TextView restTitle;

    public RestaurantsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View restaurantsView = inflater.inflate(R.layout.fragment_restaurants, container, false);

        restTitle = restaurantsView.findViewById(R.id.rest_title);

        return restaurantsView;
    }

}

package com.example.emir.placefinder;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.emir.placefinder.constants.AppConstants;
import com.example.emir.placefinder.fragment_adapters.TabsAdapter;
import com.example.emir.placefinder.fragments.CafesFragment;
import com.example.emir.placefinder.fragments.HotelsFragment;
import com.example.emir.placefinder.fragments.RestaurantsFragment;
import com.example.emir.placefinder.fragments.UsersFragment;
import com.example.emir.placefinder.model_place.CapitalCity;
import com.example.emir.placefinder.model_place.Place;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,
        TabLayout.OnTabSelectedListener, AdapterView.OnItemSelectedListener,
        GoogleMap.OnCameraIdleListener, GoogleMap.OnMarkerClickListener {

    private LoginButton fbButton;
    private Button logOffButton;
    private ImageView imgFb;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TabsAdapter tabsAdapter;
    private Spinner comboCapitalCities;

    private FirebaseAuth auth;
    private FirebaseUser firebaseUser;

    private LocationManager locationManager;
    private LocationListener locationListener;
    private Marker myMarker;
    private GoogleMap mMap;
    private LatLng defaultLocation;
    private LatLng changedLocation;

    private List<Place> places = new ArrayList<>();

    private List<Fragment> fragmentList = new ArrayList<>();
    private ArrayAdapter<String> adapterCity;

    private SharedPreferences sharedPreferences;
    private Boolean isUserLoggedIn = false;

    private List<CapitalCity> capitalCityList = new ArrayList<>();
    private List<String> capitalCities = new ArrayList<>();

    private MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        sharedPreferences = this.getSharedPreferences(AppConstants.SHARED_PREFERENCES, MODE_PRIVATE);
        isUserLoggedIn = sharedPreferences.getBoolean("isUserLoggedIn", false);

        if (!isUserLoggedIn) {
            goToLogin();
        }

        initialize();

        setupTabPager();
        setupComboBox();
    }

    public void disconnectFromFacebook(View view) {
        mp.start();

        if (isUserLoggedIn && AccessToken.getCurrentAccessToken() == null) {
            LoginManager.getInstance().logOut();
            sharedPreferences.edit().putBoolean(AppConstants.SHARED_LOGIN, false).apply();
            goToLogin();
            return;
        }

        if (AccessToken.getCurrentAccessToken() == null) {
            Toast.makeText(getApplicationContext(),"Already logged out!",Toast.LENGTH_SHORT).show();
            return; // already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                LoginManager.getInstance().logOut();
                sharedPreferences.edit().putBoolean(AppConstants.SHARED_LOGIN, false).apply();
                goToLogin();
            }
        }).executeAsync();
    }

    private void setupComboBox() {

        adapterCity = new ArrayAdapter<String>(this,R.layout.spinner_list_item,capitalCities);
        adapterCity.setDropDownViewResource(R.layout.spinner_list_item);
        comboCapitalCities.setAdapter(adapterCity);
        comboCapitalCities.setOnItemSelectedListener(this);

        getCapitalCitiesInfo(AppConstants.URL_CAPITAL_CITIES);

    }

    private void animateMapCamera(LatLng location, int timeMiliSec){
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 15), timeMiliSec, null);
    }

    private void goToLogin() {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void setupTabPager() {

        tabsAdapter = new TabsAdapter(getSupportFragmentManager(), fragmentList);
        viewPager.setAdapter(tabsAdapter);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.restoran);
        tabLayout.getTabAt(1).setIcon(R.drawable.kafe);
        tabLayout.getTabAt(2).setIcon(R.drawable.hotel);
        tabLayout.getTabAt(3).setIcon(R.drawable.korisnik);

    }

    private void initialize() {
        logOffButton = findViewById(R.id.logoff_button);
        fbButton = findViewById(R.id.fb_btn);
        imgFb = findViewById(R.id.img_fb);
        comboCapitalCities = findViewById(R.id.combo_capital_cities);
        auth = FirebaseAuth.getInstance();
        firebaseUser = auth.getCurrentUser();

        if(firebaseUser != null) {
            Picasso.with(this)
                    .load(firebaseUser.getPhotoUrl())
                    .placeholder(R.drawable.person_placeholder)
                    .into(imgFb);
        }

        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabs);

        fragmentList.add(new RestaurantsFragment());
        fragmentList.add(new CafesFragment());
        fragmentList.add(new HotelsFragment());
        fragmentList.add(new UsersFragment());

        tabLayout.addOnTabSelectedListener(this);
        defaultLocation = AppConstants.DEFAULT_LOCATION;
        changedLocation = AppConstants.DEFAULT_LOCATION;

        mp = MediaPlayer.create(this,R.raw.button_click);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        checkPermission();
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        mMap.clear();
        mMap.setOnCameraIdleListener(this);
        mMap.setOnMarkerClickListener(this);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                // kada se korisnik krece poziva se ovaj metod
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        if (Build.VERSION.SDK_INT < 23) {
            checkPermission();
        }
        else {
            if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},1);
            }
            else {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            }
        }
    }

    public void getPlacesInfo(String url) {

        RequestQueue queue = Volley.newRequestQueue(this);
        places.clear();

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null,

                (response) -> {
                    String name ="";
                    String id ="";
                    String iconUrl = "";
                    String placeId = "";
                    String vicinity = "";
                    Boolean isOpenNow = false;
                    Float rating = 0f;
                    Double lat = 0.0;
                    Double lng = 0.0;

                    try {
                        String results = response.getString("results");
                        JSONArray jsonArrayResults = new JSONArray(results);

                        for (int i = 0; i < jsonArrayResults.length(); i++) {
                            JSONObject place = jsonArrayResults.getJSONObject(i);
                            name = place.optString("name");
                            id = place.optString("id");
                            iconUrl = place.optString("icon");
                            placeId = place.optString("place_id");
                            vicinity = place.optString("vicinity");
                            if(place.has("opening_hours")) {
                                isOpenNow = place.getJSONObject("opening_hours").optBoolean("open_now");
                            }
                            else {
                                isOpenNow = null;
                            }
                            rating = ((float) place.optDouble("rating"));
                            if (place.getJSONObject("geometry") != null) {
                                lat = place.getJSONObject("geometry").getJSONObject("location").optDouble("lat");
                                lng = place.getJSONObject("geometry").getJSONObject("location").optDouble("lng");
                            }
                            LatLng location = new LatLng(lat,lng);
                            places.add(
                                    new Place(id,name,location,iconUrl,isOpenNow,placeId,rating,vicinity)
                            );

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, (error) -> {
            Toast.makeText(getApplicationContext(),"Cause: "+error.getCause() + " ; Message: " + error.getMessage(),Toast.LENGTH_SHORT).show();
        });

        // Add the request to the RequestQueue.
        queue.add(jsObjRequest);

        queue.addRequestFinishedListener((request) ->{
            mMap.clear();
            for (Place place:places) {
                myMarker = mMap.addMarker(new MarkerOptions().position(place.getLocation()).title(place.getName()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                myMarker.setTag(place.getId());
            }
        });
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        mp.start();
        selectPlacesOnTabSelected(tab);

    }

    private void selectPlacesOnTabSelected(TabLayout.Tab tab) {

        if(!defaultLocation.equals(changedLocation)) {
            defaultLocation=changedLocation;
        }

        switch (tab.getPosition()) {
            case 0: {
                getPlacesInfo(AppConstants.URL_PLACES_RESTAURANT + "&location=" + defaultLocation.latitude + "," + defaultLocation.longitude);
            }
            break;
            case 1: {
                getPlacesInfo(AppConstants.URL_PLACES_CAFES + "&location=" + defaultLocation.latitude + "," + defaultLocation.longitude);
            }
            break;
            case 2: {
                getPlacesInfo(AppConstants.URL_PLACES_HOTELS + "&location=" + defaultLocation.latitude + "," + defaultLocation.longitude);
            }
            break;
            case 3: {
                showUsersInfo();
            }
            break;
            default:
        }

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void showUsersInfo() {

        UsersInfoDialog usersInfoDialog = UsersInfoDialog.newInstance(getApplicationContext());
        usersInfoDialog.show(getFragmentManager(),"UserInfoDialog");
    }

    @Override
    public void onCameraIdle() {

        selectPlacesOnCameraIdle();
    }

    private void selectPlacesOnCameraIdle() {

        changedLocation = mMap.getCameraPosition().target;

        switch (tabLayout.getSelectedTabPosition()) {
            case 0: {
                getPlacesInfo(AppConstants.URL_BASE + "&type=restaurant&location=" + changedLocation.latitude + "," + changedLocation.longitude);
            }
            break;
            case 1: {
                getPlacesInfo(AppConstants.URL_BASE + "&type=cafe&location=" + changedLocation.latitude + "," + changedLocation.longitude);
            }
            break;
            case 2: {
                getPlacesInfo(AppConstants.URL_BASE + "&type=lodging&location=" + changedLocation.latitude + "," + changedLocation.longitude);
            }
            break;
            default:
        }

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        mp.start();
        for (Place place:places) {
            if(place.getId().equals(marker.getTag())) {
                marker.showInfoWindow();
                showPlaceInfo(place,getApplicationContext());
            }
        }
        return false;
    }

    private void showPlaceInfo (Place place, Context context) {

        PlaceInfoDialog dialog = PlaceInfoDialog.newInstance(place,context);
        dialog.show(getFragmentManager(),"placeInfoDialog");

    }

    private void getCapitalCitiesInfo(String url) {

        RequestQueue queue = Volley.newRequestQueue(this);
        capitalCities.clear();
        capitalCityList.clear();

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    JSONObject results = null;
                    JSONObject country = null;
                    JSONArray capitalGeoPosition = null;
                    String countryId = "";
                    String cityname ="";
                    Double cityLat = 0.0;
                    Double cityLng = 0.0;

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            results = response.getJSONObject("Results");
                            Iterator<String> keys = results.keys();
                            while (keys.hasNext()) {
                                countryId = keys.next();
                                country = results.getJSONObject(countryId);
                                if (!country.isNull("Capital")) {
                                    capitalGeoPosition = country.getJSONObject("Capital").getJSONArray("GeoPt");
                                    cityname = country.getJSONObject("Capital").optString("Name");
                                    cityLat = (Double) capitalGeoPosition.get(0);
                                    cityLng = (Double) capitalGeoPosition.get(1);
                                    capitalCities.add(cityname);
                                    capitalCityList.add(
                                            new CapitalCity(
                                            cityname,
                                            new LatLng(cityLat,cityLng))
                                    );
                                }
                            }
                            Collections.sort(capitalCities);
                            Collections.sort(capitalCityList);
                            capitalCities.add(0,"Tuzla");
                            capitalCityList.add(0,new CapitalCity("Tuzla",defaultLocation));
                        } catch (Exception e) {
                            Toast.makeText(MapsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // display error
                Toast.makeText(MapsActivity.this, "Error downloading country info!", Toast.LENGTH_SHORT).show();
            }
        });

        queue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<JSONObject>() {
            @Override
            public void onRequestFinished(Request<JSONObject> request) {
                animateMapCamera(defaultLocation,5000);
                adapterCity.notifyDataSetChanged();
            }
        });

        // Add the request to the RequestQueue.
        queue.add(jsObjRequest);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mp.start();
        changedLocation = capitalCityList.get(position).getCityCoordinates();
        animateMapCamera(changedLocation,4000);
        selectPlacesOnCameraIdle();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED) {
            checkPermission();
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }
    }

    private void checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
    }

}

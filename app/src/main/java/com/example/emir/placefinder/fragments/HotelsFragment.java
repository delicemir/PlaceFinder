package com.example.emir.placefinder.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.emir.placefinder.R;

public class HotelsFragment extends Fragment {

    TextView hotelsTitle;

    public HotelsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View hotelsView = inflater.inflate(R.layout.fragment_hotels, container, false);

        hotelsTitle = hotelsView.findViewById(R.id.hotels_title);

        return hotelsView;
    }

}

package com.example.emir.placefinder;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.emir.placefinder.user.User;
import com.example.emir.placefinder.user.UserRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class UsersInfoDialog extends DialogFragment {

    private User user;
    private List<User> userList = new ArrayList<>();
    private RecyclerView recyclerView;
    private Button btnOkay;
    private UserRecyclerAdapter userRecyclerAdapter;

    private DatabaseReference mDatabase;
    private static Context userContext;

    public UsersInfoDialog() {
        // Required empty public constructor
    }

    public static UsersInfoDialog newInstance(Context context) {
        userContext = context;
        return new UsersInfoDialog();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.preview_users, container, false);

        recyclerView = view.findViewById(R.id.user_recycler_list);
        btnOkay = view.findViewById(R.id.btn_user_info_okay);

        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        mDatabase = FirebaseDatabase.getInstance().getReference();

        userRecyclerAdapter = new UserRecyclerAdapter(userList,userContext);
        recyclerView.setLayoutManager(new LinearLayoutManager(userContext, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(userRecyclerAdapter);

        getAllUsers();

        this.getDialog().setCanceledOnTouchOutside(true);

        return view;
    }


    private void getAllUsers() {

        mDatabase.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userList.clear();

                for (DataSnapshot cakeSnapshot: dataSnapshot.getChildren()) {
                    user = cakeSnapshot.getValue(User.class);
                    userList.add(user);
                    System.out.println(user.toString());
                }
                userRecyclerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(userContext,databaseError.getMessage(),Toast.LENGTH_LONG).show();
            }
        });

    }

}
